/**
 * CHI
 */
const createStatementData = require('./createStatementData.js');

function statement(invoice, plays) {

    return renderPlainText(createStatementData(invoice, plays));
}

function renderPlainText(data) {
    let result = `Statement for ${data.customer}\n`;
    for (let perf of data.performances) {
        // print line for this orde2
        result += ` ${perf.play.name}: ${usd(perf.amount)} (${perf.audience} seats)\n`;
    }

    result += `Amount owed is ${usd(data.totalAmount)}\n`;
    result += `You earned ${data.totalVolumeCredits} credits\n`;

    return result;

    function usd(aNumber) {
        return new Intl.NumberFormat(
            'en-US',
            {
                style: 'currency',
                currency: 'USD',
                minimumFractionDigits: 2
            }
        ).format(aNumber/100);
    }
}

const invoices = require('./invoices.json');
const plays = require('./plays.json');

const s = statement(invoices[0], plays);
console.log(s);
